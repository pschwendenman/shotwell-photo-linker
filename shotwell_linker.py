'''
Create a psuedo directory of shotwell information

Using the shotwell managed database of stars and tags create directory structure
with links to the real files
'''
import sqlite3
import os
import logging

LOGGER = logging.getLogger(__name__)
RATING = 5
HOME = os.environ['HOME']
PICTURE_DIR = "Pictures/shotwell/"
PHOTO_DB = '.local/share/shotwell/data/photo.db'

class TwoWayDict(dict):
    def __setitem__(self, key, value):
        # Remove any previous connections with these values
        if key in self:
            del self[key]
        if value in self:
            del self[value]
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)

    def __len__(self):
        """Returns the number of connections"""
        return dict.__len__(self) // 2

def shotwell_linker(rating):
    '''
    The shotwell linker fuction
    '''
    photo_db = os.path.join(HOME, PHOTO_DB)
    if not os.path.exists(photo_db):
        raise NameError('Photo db does not exist: %s', photo_db)
    with sqlite3.connect(photo_db) as conn:
        cursor = conn.cursor()

        cursor.execute('SELECT filename, timestamp FROM PhotoTable WHERE rating=?', (rating,))
        photos = cursor.fetchall()


    dir_path = os.path.join(HOME, PICTURE_DIR, 'rating', str(rating))

    if not os.path.exists(dir_path):
        LOGGER.info("Making directoy %s", dir_path)
        os.makedirs(dir_path)
    else:
        LOGGER.info("Directoy exists %s", os.path.abspath(dir_path))

    for photo, timestamp in photos:
        root, ext = os.path.splitext(os.path.basename(photo))
        filename = ''.join([root, '_', str(timestamp), ext])
        if os.path.exists(os.path.join(dir_path, filename)):
            LOGGER.info('Skipping %s', filename)
        else:
            LOGGER.info("Linking %s to %s", photo,
                        os.path.join(dir_path, filename))
            os.link(photo, os.path.join(dir_path, filename))


def main():
    '''
    call shotwell_linker
    '''
    stream = logging.StreamHandler()
    stream.setLevel(logging.DEBUG)
    formatter_simple = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    stream.setFormatter(formatter_simple)
    LOGGER.addHandler(stream)
    LOGGER.setLevel(logging.DEBUG)

    shotwell_linker(5)
    shotwell_linker(4)
    shotwell_linker(3)
    #shotwell_linker(2)
    #shotwell_linker(1)

if __name__ == '__main__':
    main()
